// package rtlsdr implement a Go binding to the librtlsdr C library.
// the library C sources are included in this package

package rtlsdr

// // Include files needed by the Go binding
// #include <stdlib.h>
// #include "rtl-sdr.h"
//
// // Declaration of C callback bridge function defined in "cfuncs.go"
// void bridgeReadAsyncCB(unsigned char* buf, uint32_t len, void *ctx);
import "C"

import (
	"fmt"
	"strings"
	"unsafe"
)

//
// Tuner types
//
type TunerType int

const (
	TunerUnknown TunerType = C.RTLSDR_TUNER_UNKNOWN
	TunerE400    TunerType = C.RTLSDR_TUNER_E4000
	TunerFC0012  TunerType = C.RTLSDR_TUNER_FC0012
	TunerFC0013  TunerType = C.RTLSDR_TUNER_FC0013
	TunerFC2580  TunerType = C.RTLSDR_TUNER_FC2580
	TunerR820T   TunerType = C.RTLSDR_TUNER_R820T
	TunerR828D   TunerType = C.RTLSDR_TUNER_R828D
)

// Device encapsulates a C rtlsdr device
type Device struct {
	cdev   *C.rtlsdr_dev_t  // Pointer to C device handle
	readCB func(buf []byte) // Callback function for ReadSync
}

// Map of device C pointers to Go Device pointer
// Used by callback functions
var mapDevices = map[*C.rtlsdr_dev_t]*Device{}

// GetDeviceCount returns the number of recognized devices
func GetDeviceCount() uint {

	return uint(C.rtlsdr_get_device_count())
}

// GetDeviceName returns the name of the device with the specified index
func GetDeviceName(index uint) string {

	pname := C.rtlsdr_get_device_name(C.uint32_t(index))
	return C.GoString(pname)
}

// GetDeviceUsbStrings returns USB strings for the device specified
// by its index
func GetDeviceUsbStrings(index uint) (manuf, product, serial string, err error) {

	cmanuf := C.CString(strings.Repeat(" ", 256))
	cproduct := C.CString(strings.Repeat(" ", 256))
	cserial := C.CString(strings.Repeat(" ", 256))
	defer C.free(unsafe.Pointer(cmanuf))
	defer C.free(unsafe.Pointer(cproduct))
	defer C.free(unsafe.Pointer(cserial))

	cerr := C.rtlsdr_get_device_usb_strings(C.uint32_t(index), cmanuf, cproduct, cserial)
	if cerr != 0 {
		return "", "", "", fmt.Errorf("Error %d", cerr)
	}
	return C.GoString(cmanuf), C.GoString(cproduct), C.GoString(cserial), nil
}

// GetIndexBySerial returns the index of the device by it serial id
func GetIndexBySerial(serial string) int {

	cserial := C.CString(serial)
	defer C.free(unsafe.Pointer(cserial))
	return int(C.rtlsdr_get_index_by_serial(cserial))
}

// Open try to open the device with the specified index and
// returns the device object if successfull
func Open(index uint) (*Device, error) {

	var cdev *C.rtlsdr_dev_t
	cerr := C.rtlsdr_open(&cdev, C.uint32_t(index))
	if cerr != 0 {
		return nil, fmt.Errorf("Error:%d opening device", cerr)
	}

	dev := &Device{cdev, nil}
	mapDevices[cdev] = dev
	return dev, nil
}

// Close closes the specified previously opened device
func Close(dev *Device) error {

	cerr := C.rtlsdr_close(dev.cdev)
	delete(mapDevices, dev.cdev)
	if cerr == 0 {
		return nil
	}
	return fmt.Errorf("Error:%d closing device", cerr)
}

// SetXtalFreq sets crystal oscillator frequencies used for the RTL2832 and the tuner IC.
// Usually both ICs use the same clock. Changing the clock may make sense if
// you are applying an external clock to the tuner or to compensate the
// frequency (and samplerate) error caused by the original (cheap) crystal.
// NOTE: Call this function only if you fully understand the implications.
// dev:        the device handle given by rtlsdr_open()
// rtl_freq:   frequency value used to clock the RTL2832 in Hz
// tuner_freq: frequency value used to clock the tuner IC in Hz
// return 0 on success
func SetXtalFreq(dev *Device, rtl_freq uint, tuner_freq uint) error {

	cerr := C.rtlsdr_set_xtal_freq(dev.cdev, C.uint32_t(rtl_freq), C.uint32_t(tuner_freq))
	if cerr == 0 {
		return nil
	}
	return fmt.Errorf("Error:%d setting crystal oscillator frequency", cerr)
}

// GetUsbString returns the following strings for the specified opened device
// manuf:   manufacturer name, may be ""
// product: product name, may be ""
// serial:  serial number, may be ""
func GetUsbStrings(dev *Device) (manuf, product, serial string, err error) {

	cmanuf := C.CString(strings.Repeat(" ", 256))
	cproduct := C.CString(strings.Repeat(" ", 256))
	cserial := C.CString(strings.Repeat(" ", 256))
	defer C.free(unsafe.Pointer(cmanuf))
	defer C.free(unsafe.Pointer(cproduct))
	defer C.free(unsafe.Pointer(cserial))

	cerr := C.rtlsdr_get_usb_strings(dev.cdev, cmanuf, cproduct, cserial)
	if cerr != 0 {
		return "", "", "", fmt.Errorf("Error %d", err)
	}
	return C.GoString(cmanuf), C.GoString(cproduct), C.GoString(cserial), nil
}

// SetCenterFreq sets the center frequency of the tuner in hertz
func SetCenterFreq(dev *Device, freq uint32) error {

	cerr := C.rtlsdr_set_center_freq(dev.cdev, C.uint32_t(freq))
	if cerr == 0 {
		return nil
	}
	return fmt.Errorf("Error:%d setting center frequency", cerr)
}

// GetCenterFreq gets the center frequency of the tuner in hertz
func GetCenterFreq(dev *Device) (freq uint32) {

	cfreq := C.rtlsdr_get_center_freq(dev.cdev)
	return uint32(cfreq)
}

// SetFreqCorrelation sets the frequency correction value for the device.
// ppm: correction value in parts per million (ppm)
// return 0 on success
func SetFreqCorrection(dev *Device, ppm int) error {

	cerr := C.rtlsdr_set_freq_correction(dev.cdev, C.int(ppm))
	if cerr == 0 {
		return nil
	}
	return fmt.Errorf("Error:%d setting frequency correlation", cerr)
}

// GetFreqCorrelation returns the actual frequency correlation of the device
// in parts per million (ppm)
func GetFreqCorrection(dev *Device) (fcor int) {

	cfcor := C.rtlsdr_get_freq_correction(dev.cdev)
	return int(cfcor)
}

// GetTunerType returns the tuner type for the specified device
// or TunerUnknown if error
func GetTunerType(dev *Device) TunerType {

	ctype := C.rtlsdr_get_tuner_type(dev.cdev)
	return TunerType(ctype)
}

// GetTunerGains returns list of gains supported by the tuner for the specified device
// The gains are returned inn tenths of a dB, 115 means 11.5 dB.
func GetTunerGains(dev *Device) (gains []int, err error) {

	// Get total number of gains
	count := int(C.rtlsdr_get_tuner_gains(dev.cdev, nil))
	fmt.Printf("gains:%v\n", count)
	if count < 0 {
		return nil, fmt.Errorf("Error getting tuner gains")
	}

	// Get array of gains
	cgains := make([]C.int, count)
	count = int(C.rtlsdr_get_tuner_gains(dev.cdev, (*C.int)(unsafe.Pointer(&cgains[0]))))
	if count < 0 {
		return nil, fmt.Errorf("Error getting tuner gains")
	}
	gains = make([]int, count)
	for i := 0; i < count; i++ {
		gains[i] = int(cgains[i])
	}
	return gains, nil
}

// SetTunerGain sets the tuner gain for the specified device
// Manual gain mode must be enabled for this to work.
// Gains are specified in tenths of a dB, 115 means 11.5 dB.
func SetTunerGain(dev *Device, gain int) error {

	cerr := C.rtlsdr_set_tuner_gain(dev.cdev, C.int(gain))
	if cerr == 0 {
		return nil
	}
	return fmt.Errorf("Error:%d setting tuner gain", cerr)
}

// SetTunerBandwidth sets the tuner bandwith in Hz for the specified device
// Zero means automatic BW selection.
func SetTunerBandwidth(dev *Device, bw uint) error {

	cerr := C.rtlsdr_set_tuner_bandwidth(dev.cdev, C.uint32_t(bw))
	if cerr == 0 {
		return nil
	}
	return fmt.Errorf("Error:%d setting tuner bandwidth", cerr)
}

// GetTunerGain returns the actual gain the device is configured to.
// The return gains is specfied in tenths of a dB, 115 means 11.5 dB.
func GetTunerGain(dev *Device) (gain int) {

	cgain := C.rtlsdr_get_tuner_gain(dev.cdev)
	return int(cgain)
}

// SetTunerIfGain sets the intermediate frequency gain for the specified device and stage
// Stage number should be 1 to 6 for E4000
// Gain is specified in tenths of a dB, -30 means -3.0 dB.
func SetTunerIfGain(dev *Device, stage, gain int) error {

	cerr := C.rtlsdr_set_tuner_if_gain(dev.cdev, C.int(stage), C.int(gain))
	if cerr == 0 {
		return nil
	}
	return fmt.Errorf("Error:%d setting tuner if gain", cerr)
}

// SetTunerGainMode set the gain mode (automatic/manual) for the specified device
// Manual gain mode must be enabled for the gain setter function to work.
func SetTunerGainMode(dev *Device, manual bool) error {

	var cmanual C.int
	if manual {
		cmanual = 1
	} else {
		cmanual = 0
	}
	cerr := C.rtlsdr_set_tuner_gain_mode(dev.cdev, cmanual)
	if cerr == 0 {
		return nil
	}
	return fmt.Errorf("Error:%d setting tuner gain mode", cerr)
}

// SetSampleRate sets the sample rate for the device, also selects the baseband filters
// according to the requested sample rate for tuners where this is possible.
// Sample rate possible values are:
//   225001 - 300000 Hz
//   900001 - 3200000 Hz
// sample loss is to be expected for rates > 2400000
func SetSampleRate(dev *Device, rate uint) error {

	cerr := C.rtlsdr_set_sample_rate(dev.cdev, C.uint32_t(rate))
	if cerr == 0 {
		return nil
	}
	return fmt.Errorf("Error:%d setting sample rate", cerr)
}

// GetSampleRate returns the actual sample rate in Hz the device is configured to.
func GetSampleRate(dev *Device) (sr uint, err error) {

	cres := C.rtlsdr_get_sample_rate(dev.cdev)
	if cres < 0 {
		return 0, fmt.Errorf("Error:%d getting sample rate", cres)
	}
	return uint(cres), nil
}

// SetTestmode enables test mode that returns an 8 bit counter instead of the samples
// The counter is generated inside the RTL2832.
func SetTestmode(dev *Device, on bool) error {

	cerr := C.rtlsdr_set_testmode(dev.cdev, bool2cint(on))
	if cerr == 0 {
		return nil
	}
	return fmt.Errorf("Error:%d setting test mode", cerr)
}

// SetAgcMode enable or disable the internal digital AGC of the RTL2832.
func SetAgcMode(dev *Device, on bool) error {

	cerr := C.rtlsdr_set_agc_mode(dev.cdev, bool2cint(on))
	if cerr == 0 {
		return nil
	}
	return fmt.Errorf("Error:%d setting test mode", cerr)
}

// SetDirectSampling enable or disable the direct sampling mode.
// When enabled, the IF mode of the RTL2832 is activated, and SetCenterFreq() will control
// the IF-frequency of the DDC, which can be used to tune from 0 to 28.8 MHz
// (xtal frequency of the RTL2832).
// on 0 means disabled, 1 I-ADC input enabled, 2 Q-ADC input enabled
func SetDirectSampling(dev *Device, on int) error {

	cerr := C.rtlsdr_set_direct_sampling(dev.cdev, C.int(on))
	if cerr == 0 {
		return nil
	}
	return fmt.Errorf("Error:%d setting direct sampling", cerr)
}

// GetDirectSampling returns the state of the direct sampling mode
// return -1 on error, 0 means disabled, 1 I-ADC input enabled
//         2 Q-ADC input enabled
func GetDirectSampling(dev *Device) int {

	cres := C.rtlsdr_get_direct_sampling(dev.cdev)
	return int(cres)
}

// SetOffSetTuning enable or disable offset tuning for zero-IF tuners,
// which allows to avoid problems caused by the DC offset of the ADCs and 1/f noise.
func SetOffsetTuning(dev *Device, on bool) error {

	cerr := C.rtlsdr_set_offset_tuning(dev.cdev, bool2cint(on))
	if cerr == 0 {
		return nil
	}
	return fmt.Errorf("Error:%d setting offset tuning", cerr)
}

// GetOffsetTuning returns the state of the offset tuning mode
func GetOffsetTuning(dev *Device) (on bool, err error) {

	cres := C.rtlsdr_get_offset_tuning(dev.cdev)
	if cres == 0 {
		return false, nil
	}
	if cres == 1 {
		return true, nil
	}
	return false, fmt.Errorf("Error getting offset tuning")
}

// ResetBuffer should normally be called before starting to read data from the device
func ResetBuffer(dev *Device) error {

	cerr := C.rtlsdr_reset_buffer(dev.cdev)
	if cerr == 0 {
		return nil
	}
	return fmt.Errorf("Error:%d resetting buffer", cerr)
}

// ReadSync read samples from the device synchronously
// Up to len(buf) bytes are into the specified buffer and the number
// of bytes read and an eventual error are returned
func ReadSync(dev *Device, buf []byte) (n int, err error) {

	var nread C.int
	cerr := C.rtlsdr_read_sync(dev.cdev, unsafe.Pointer(&buf[0]), C.int(len(buf)), &nread)
	if cerr == 0 {
		return int(nread), nil
	}
	return 0, fmt.Errorf("Error:%d reading sync", cerr)
}

// goReadAsyncCB is a callback called by 'bridgeReadAsyncCB' defined in "cfuncs.go"
// when data from the device is received
//export goReadAsyncCB
func goReadAsyncCB(buf unsafe.Pointer, nbytes int, ctx unsafe.Pointer) {

	// Get the Go device from the received context
	cdev := (*C.rtlsdr_dev_t)(ctx)
	dev, ok := mapDevices[cdev]
	if !ok {
		panic("goReadAsyncCB: device not found")
	}

	// Call Go callback passing the buffer with received data as a slice
	slice := (*[1 << 30]byte)(unsafe.Pointer(buf))[:nbytes:nbytes]
	dev.readCB(slice)
}

// ReadAsync read samples from the device asynchronously calling the specified
// callback with the read data. This function will block until
// it is being canceled using CancelAsync()
//
// bufNum is the optional number of buffers which will be allocated
// Set to 0 for the default buffer count of 15
//
// bufLen is the optional buffer length, must be a multiple of 512,
// should be a multiple of 16384 (URB size)
// Set to 0 for the default buffer length of 16*32*512 = 16384
//
// The overall buffer size will be bufNum * bufLen
func ReadAsync(dev *Device, bufNum int, bufLen int, readCB func(buf []byte)) error {

	dev.readCB = readCB
	cerr := C.rtlsdr_read_async(
		dev.cdev,
		(C.rtlsdr_read_async_cb_t)(unsafe.Pointer(C.bridgeReadAsyncCB)),
		unsafe.Pointer(dev.cdev),
		C.uint32_t(bufNum),
		C.uint32_t(bufLen),
	)
	if cerr == 0 {
		return nil
	}
	return fmt.Errorf("Error:%d reading async", cerr)
}

// CancelAsync cancel all pending asynchronous operations on the device.
func CancelAsync(dev *Device) error {

	cerr := C.rtlsdr_cancel_async(dev.cdev)
	if cerr == 0 {
		return nil
	}
	return fmt.Errorf("Error:%d canceling async", cerr)
}

// bool2cint converts a Go bool to a C int
func bool2cint(v bool) C.int {

	if v {
		return 1
	} else {
		return 0
	}
}
