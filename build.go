package rtlsdr

/*
// Linux build flags
#cgo linux CFLAGS:  -DDETACH_KERNEL_DRIVER -I ${SRCDIR}/librtlsdr/include -I /usr/include/libusb-1.0
#cgo linux LDFLAGS: -lusb-1.0

// Windows build flags
#cgo windows CFLAGS: -I ${SRCDIR}/librtlsdr/include -I ${SRCDIR}/win64
#cgo windows LDFLAGS: -L ${SRCDIR}/win64 -llibusb-1.0

// Include rtl-sdr library sources
#include "librtlsdr/src/librtlsdr.c"
#include "librtlsdr/src/tuner_e4k.c"
#include "librtlsdr/src/tuner_fc0012.c"
#include "librtlsdr/src/tuner_fc0013.c"
#include "librtlsdr/src/tuner_fc2580.c"
#include "librtlsdr/src/tuner_r82xx.c"
*/
import "C"
