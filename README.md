# GoRTLSDR - Go bindings for librtlsdr

This package implements a Go binding for the [librtlsdr](https://bitbucket.org/steve-m/librtlsdr) C library
which allows a Realtek RTL2832 based DBV Dongle to be used as a SDR (Software Defined Radio) receiver.
It includes the sources of the C library and depends only on the libusb library.

Note that the librtlsdr licence is GPL.

To install it is necessary to have a gcc compatible toolchain in the path.
A test program [rtlsdrltest](https://bitbucket.org/leonsal/gortlsdr/tree/master/rtlsdrtest)
program exercises most functions of the binding. To install it:

`>go install bitbucket.org/leonsal/gortlsdr/rtlsdrtest`



