package main

import (
	sdr "bitbucket.org/leonsal/gortlsdr"
	"flag"
	"fmt"
)

// Command line options
var (
	oIndex = flag.Uint("index", 0, "Device index")
)

func main() {

	// Parse command line parameters
	flag.Parse()

	// Get number of RTLSDR devices
	count := sdr.GetDeviceCount()
	fmt.Printf("Number of RTLSDR devices found:%d\n", count)
	if count == 0 {
		return
	}

	// List devices info
	for idx := 0; idx < int(count); idx++ {
		manuf, product, serial, err := sdr.GetDeviceUsbStrings(0)
		if err != nil {
			panic(err)
		}
		fmt.Printf("\tIndex:%d Manufacturer:%s Product:%s Serial:%s\n", idx, manuf, product, serial)
	}

	// Opens first device
	fmt.Printf("Opening device: 0\n")
	dev, err := sdr.Open(*oIndex)
	if err != nil {
		panic(err)
	}

	// Get and shows the tuner type
	tuner := sdr.GetTunerType(dev)
	switch tuner {
	case sdr.TunerUnknown:
		fmt.Printf("Tuner: Unknown\n")
	case sdr.TunerE400:
		fmt.Printf("Tuner: E400\n")
	case sdr.TunerFC0012:
		fmt.Printf("Tuner: FC0012\n")
	case sdr.TunerFC0013:
		fmt.Printf("Tuner: FC0013\n")
	case sdr.TunerFC2580:
		fmt.Printf("Tuner: FC2580\n")
	case sdr.TunerR820T:
		fmt.Printf("Tuner: R820T\n")
	case sdr.TunerR828D:
		fmt.Printf("Tuner: R828D\n")
	default:
		panic("Invalid tuner type")
	}

	// Get and shows all tuner gains
	gains, err := sdr.GetTunerGains(dev)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Tuner gains(%d): %v\n", len(gains), gains)

	// Get and show current tuner gain
	gain := sdr.GetTunerGain(dev)
	fmt.Printf("Tuner current gain:%v\n", gain)

	// Get and show current sample rate
	sr, err := sdr.GetSampleRate(dev)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Current sample rate:%v\n", sr)

	// Get and show current center frequency
	freq := sdr.GetCenterFreq(dev)
	fmt.Printf("Current center frequency:%v\n", freq)

	// Get and show current frequency correction
	fcor := sdr.GetFreqCorrection(dev)
	fmt.Printf("Current frequency correction:%v\n", fcor)

	// Get and show direct sampling mode
	dirs := sdr.GetDirectSampling(dev)
	fmt.Printf("Direct sampling:%v\n", dirs)

	// Get and show offset tuning state
	offtuning, err := sdr.GetOffsetTuning(dev)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Offset tuning:%v\n\n", offtuning)

	// Executes test mode synchronously
	fmt.Printf("Test Mode Sync (probably has errors):\n")
	err = testModeSync(dev, 10)
	if err != nil {
		panic(err)
	}

	// Executes test mode asynchronously
	fmt.Printf("Test Mode Async (should not have errors):\n")
	err = testModeAsync(dev, 10)
	if err != nil {
		panic(err)
	}

	// Executes reception
	fmt.Printf("Reception...\n")
	err = testRx(dev, 20)
	if err != nil {
		panic(err)
	}

	// Close device
	err = sdr.Close(dev)
	if err != nil {
		panic(err)
	}
}

// Execute test mode asynchronously for the specified number of read calls
func testModeAsync(dev *sdr.Device, count int) error {

	// Enter into test mode
	err := sdr.SetTestmode(dev, true)
	if err != nil {
		return err
	}

	// Needs to reset buffer before calling ReadAsync
	err = sdr.ResetBuffer(dev)
	if err != nil {
		return err
	}

	// Start receiving test data from the device
	// The data is sequential bytes: 0, 1, 2, .. 255, 0, 1, 2
	tn := 1
	bufNum := 15
	bufLen := 16 * 32 * 512
	errors := 0
	var last byte
	err = sdr.ReadAsync(dev, bufNum, bufLen, func(buf []byte) {
		// Shows received buffer info
		fmt.Printf("ReadAsync(%d) len:%v data:%v\n", tn, len(buf), buf[0:20])
		// Checks received block size
		if len(buf) < bufLen {
			sdr.CancelAsync(dev)
			panic(fmt.Sprintf("Short read(%v/%v), samples lost, exiting!", len(buf), bufLen))
		}
		// Checks buffer bytes
		if tn > 1 && buf[0] != last+1 {
			errors++
		}
		for i := 1; i < len(buf); i++ {
			if buf[i] != buf[i-1]+1 {
				errors++
			}
		}
		last = buf[len(buf)-1]
		// If test number reaches the specified count, uses CancelAsync()
		// to terminate ReadAsync()
		tn++
		if tn >= count {
			err := sdr.CancelAsync(dev)
			if err != nil {
				panic(err)
			}
		}
	})
	if err != nil {
		return err
	}

	// Leave test mode
	err = sdr.SetTestmode(dev, false)
	if err != nil {
		return err
	}
	fmt.Printf("ReadAsync errors:%v\n\n", errors)
	return nil
}

// Execute test mode synchronously for the specified number of read calls
func testModeSync(dev *sdr.Device, count int) error {

	// Enter into test mode
	err := sdr.SetTestmode(dev, true)
	if err != nil {
		return err
	}

	// Needs to reset buffer before calling ReadAsync
	err = sdr.ResetBuffer(dev)
	if err != nil {
		return err
	}

	// Reads test data from the device
	// The data is sequential bytes: ..., 1, 2, .. 255, 0, 1, 2, ...
	tn := 1
	bufLen := 16 * 32 * 512
	buf := make([]byte, bufLen)
	errors := 0
	var last byte
	for tn <= count {
		n, err := sdr.ReadSync(dev, buf)
		if err != nil {
			return err
		}
		fmt.Printf("ReadSync(%d) len:%v read:%v data:%v\n", tn, len(buf), n, buf[0:20])
		// Checks buffer bytes
		if tn > 1 && buf[0] != last+1 {
			errors++
		}
		for i := 1; i < len(buf); i++ {
			if buf[i] != buf[i-1]+1 {
				errors++
			}
		}
		last = buf[len(buf)-1]
		tn++
	}

	// Leave test mode
	err = sdr.SetTestmode(dev, false)
	if err != nil {
		return err
	}
	fmt.Printf("ReadSync errors:%v\n\n", errors)
	return nil
}

func testRx(dev *sdr.Device, count int) error {

	// Sets and checks frequency correction in ppm
	ppm := 10
	err := sdr.SetFreqCorrection(dev, ppm)
	if err != nil {
		return err
	}
	ppm2 := sdr.GetFreqCorrection(dev)
	if ppm2 != ppm {
		return fmt.Errorf("Frequency correction different from set: %v/%v\n", ppm2, ppm)
	}

	// Sets and checks the center frequency
	freq := uint32(91000000)
	err = sdr.SetCenterFreq(dev, freq)
	if err != nil {
		return err
	}
	freq2 := sdr.GetCenterFreq(dev)
	if freq != freq2 {
		return fmt.Errorf("Frequency different from set: %v/%v\n", freq2, freq)
	}

	// Sets and checks the sample rate
	rate := uint(2800000)
	err = sdr.SetSampleRate(dev, rate)
	if err != nil {
		return err
	}
	rate2, err := sdr.GetSampleRate(dev)
	if rate2 != rate {
		return fmt.Errorf("Samplerate different from set: %v/%v\n", rate2, rate)
	}

	// Sets automatic bandwidth selection
	err = sdr.SetTunerBandwidth(dev, 0)
	if err != nil {
		return err
	}

	// Sets tuner gain to manual mode
	err = sdr.SetTunerGainMode(dev, true)
	if err != nil {
		return err
	}

	// Sets and checks manual tuner gain
	tgain := 125
	err = sdr.SetTunerGain(dev, tgain)
	if err != nil {
		return err
	}
	tgain2 := sdr.GetTunerGain(dev)
	if tgain2 != tgain {
		return fmt.Errorf("Tuner gain different from set: %v/%v\n", tgain2, tgain)
	}

	// Sets tuner gain to auto mode
	err = sdr.SetTunerGainMode(dev, false)
	if err != nil {
		return err
	}

	// Disables AGC mode
	err = sdr.SetAgcMode(dev, false)
	if err != nil {
		return err
	}

	// Enables AGC mode
	err = sdr.SetAgcMode(dev, true)
	if err != nil {
		return err
	}

	// Needs to reset buffer before calling ReadAsync
	err = sdr.ResetBuffer(dev)
	if err != nil {
		return err
	}

	// Start receiving data from the device
	tn := 1
	bufNum := 15
	bufLen := 16 * 32 * 512
	err = sdr.ReadAsync(dev, bufNum, bufLen, func(buf []byte) {
		// Shows received buffer info
		fmt.Printf("ReadAsync(%d) len:%v data:%v\n", tn, len(buf), buf[0:20])
		// Checks received block size
		if len(buf) < bufLen {
			sdr.CancelAsync(dev)
			panic(fmt.Sprintf("Short read(%v/%v), samples lost, exiting!", len(buf), bufLen))
		}
		// If test number reaches the specified count, uses CancelAsync()
		// to terminate ReadAsync()
		tn++
		if tn >= count {
			err := sdr.CancelAsync(dev)
			if err != nil {
				panic(err)
			}
		}
	})
	if err != nil {
		return err
	}

	return nil
}
